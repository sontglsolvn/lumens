import React from 'react';
import './CarouselCard.css';

const CarouselCard = ({ coinInfo }) => {
  return (
    <div className='WrapCarouselCard'>
      <img
        src={`${coinInfo.urlImage}`}
        alt={coinInfo?.countCoin}
        style={{ width: '100%' }}
      ></img>
      <span className={coinInfo?.isWaiting ? 'CountCoin_disable' : 'CountCoin'}>
        {coinInfo?.isWaiting && <img src='/clock.png' alt='clock'></img>}{' '}
        {coinInfo?.countCoin}
      </span>
      <span className='InfoCoin'>{coinInfo?.details}</span>
      {coinInfo?.isWaiting && (
        <a href='/' className='Insufficient'>
          Insufficient coins
        </a>
      )}
    </div>
  );
};
export default CarouselCard;
