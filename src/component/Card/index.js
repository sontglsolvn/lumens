import React from 'react';
import './card.css';

const CardCoupons = () => {
  return (
    <div className='WrapCard'>
      <div className='WrapContent'>
        <span className='TitleCoin'>Available Coin balance</span>
        <span className='Count'>340</span>
        <div class='ProgressBar'>
          <span style={{ width: '65%' }}></span>
        </div>
        <span className='Information'>
          You have paid rental fee for $1,200. <br />
          Pay more $800 to achieve Gold Tier.
        </span>
        <a className='ViewDetails' href='/'>{`View tier benefits >`}</a>
        <button class='BtnCoupons'>My Coupons</button>
        <span className='TimeUpdate'>Updated : 02/11/2021</span>
      </div>
    </div>
  );
};

export default CardCoupons;
