import React from 'react';
import './topic.css';
import CarouselCard from '../CarouselCard';

const Topic = ({ item }) => {
  const { topicTitle, listCoint } = item;
  return (
    <div key={topicTitle} className='WrapTopic'>
      <span className='TitleTopic'>{topicTitle}</span>
      <div className='Carousel'>
        {listCoint?.map((itemCoin) => (
          <CarouselCard coinInfo={itemCoin} />
        ))}
      </div>
    </div>
  );
};
export default Topic;
