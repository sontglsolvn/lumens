import React from 'react';
import './button.css';

const ButtonCustom = (props) => {
  return (
    <div className='button'>
      <div className={`icon${props.name}`}> </div>
    </div>
  );
};

export default ButtonCustom;
