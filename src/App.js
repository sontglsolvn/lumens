import './App.css';

import ButtonCustom from '../src/component/Button';
import CardCoupons from '../src/component/Card';
import Topic from '../src/component/Topic';

const ListTopic = [
  {
    topicTitle: 'Petrol',
    listCoint: [
      {
        urlImage: '/image001.png',
        countCoin: '15 Coins',
        details: '50% discount for every $100 top-up on your Shell Petrol Card',
        isWaiting: false,
      },
      {
        urlImage: '/image002.png',
        countCoin: '1,000 Coins',
        details: '70% discount top-up on your Shell Petrol Card',
        isWaiting: true,
      },
    ],
  },
  {
    topicTitle: 'Rental Rebate',
    listCoint: [
      {
        urlImage: '/image003.png',
        countCoin: '20 Coins',
        details: 'Get $20 Rental rebate',
        isWaiting: false,
      },
      {
        urlImage: '/image001.png',
        countCoin: '15 Coins',
        details: 'Get $500 Rental rebate',
        isWaiting: false,
      },
    ],
  },
  {
    topicTitle: 'Food and Beverage',
    listCoint: [
      {
        urlImage: '/image002.png',
        countCoin: '25 Coins',
        details: 'NTUC Fairprice $50 Voucher',
        isWaiting: false,
      },
      {
        urlImage: '/image003.png',
        countCoin: '5 Coins',
        details: 'Free Cold Stone Sundae at any flavour!',
        isWaiting: false,
      },
    ],
  },
];
function App() {
  return (
    <div className='Wrapper'>
      <div className='Content'>
        <div className='Banner'>
          <div className='WrapButton'>
            <button class='ButtonRound'>
              <div className='IconBack'></div>
            </button>
          </div>
          <span className='Title'> Silver Tier</span>
          <span className='SubTitle'>
            In Silver Tier, every $1 in rental fee paid, you get 2 coins to
            redeem exclusive rewards.
          </span>
          <CardCoupons />
        </div>
        <div className='WrapContentTopic'>
          {ListTopic.map((item) => (
            <Topic item={item} />
          ))}
        </div>
      </div>
      <div className='Footer'>
        <ButtonCustom name='Home' />
        <ButtonCustom name='Notification' />
        <ButtonCustom name='Pay' />
        <ButtonCustom name='Account' />
      </div>
    </div>
  );
}

export default App;
